all_math_functions = ['+', '-', '/', '*']

def num_parse(numbers):
    string_list = []
    count = 0
    tmp = ''

    for char in numbers:
        count += 1
        if char not in all_math_functions:
            tmp += char

        if char in all_math_functions or count == len(numbers):
            string_list.append(tmp)
            tmp = ''
    digit_list = [int(num) for num in string_list]
    return digit_list
#

def func_parse(numbers):

    func_list = []
    for func in numbers:
        if func in all_math_functions:
            func_list.append(func)

    return func_list